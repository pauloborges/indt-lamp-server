var _ = require('underscore');

var Lamp = function (socket) {
  this.socket = socket;
  this.id = undefined;
  this.name = undefined;
  this.state = undefined;
  this.color = undefined;
};

Lamp.prototype.resource = function () {
  return _.pick(this, 'id', 'name', 'state', 'color');
};

module.exports = Lamp;
