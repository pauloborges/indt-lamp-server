Protocol = {
  OK:     0x0,
  NO:     0x1,
  PING:   0x2,
  PONG:   0x3,
  ALIVE:  0x4,
  ON:     0x5,
  OFF:    0x6,
  COLOR:  0x7
};

module.exports = Protocol;
