var format = require('util').format;
var express = require('express');
var net = require('net');

var _ = require('underscore');
var Lamp = require('./lamp');
var protocol = require('./protocol');

var api = express();
var lamps = {};
var lastId = 0;

api.get('/lamps', function (req, res) {
  res.json(_.map(_.values(lamps), function (lamp) {
    return lamp.resource();
  }));
});

api.get('/lamps/:id(\\d+)', function (req, res) {
  if (!lamps[req.params.id]) {
    res.status(404).json({reason: 'id not found'});
    return;
  }

  res.json(lamps[req.params.id].resource());
});

api.get('/lamps/:id(\\d+)/:onoff(on|off)', function (req, res) {
  var lamp = lamps[req.params.id];

  if (!lamp) {
    res.status(404).json({reason: 'id not found'});
    return;
  }

  var onoff = {
    on: Protocol.ON,
    off: Protocol.OFF
  }[req.params.onoff];

  var msg = new Buffer([2, onoff]);
  lamp.socket.write(msg);

  console.log(format('[lamp %d] <<< %s'), lamp.id, msg.toString('hex'));
  res.json({reason: 'ok'});
});

net.createServer(function (socket) {
  var lamp = new Lamp(socket);

  var buf = Buffer(512);
  var buflen = 0;
  buf.fill(0);

  socket.setTimeout(30000);

  socket.on('data', function (chunk) {
    console.log(format('[lamp %d] >>> %s'), lamp.id, chunk.toString('hex'));

    // Append chunk into message buffer
    chunk.copy(buf, buflen);
    buflen += chunk.length;

    // Process all messages
    while (1) {
      if (buflen == 0)
        break;

      // Check if there is a complete message
      var messageLen = buf.readUInt8(0);
      if (buflen < messageLen)
        break;

      // Extract opcode and arguments
      var opcode = buf.readUInt8(1);
      var argsLen = messageLen - 2;
      var args = new Buffer(argsLen);

      if (argsLen > 0)
        buf.copy(args, 0, 2, messageLen);

      // Process message
      if (opcode == Protocol.ALIVE) {
        lamp.id = lastId++;
        lamp.name = args.toString();
        lamp.state = 'off';
        lamp.color = [0, 0, 0];

        lamps[lamp.id] = lamp;

        var resp = new Buffer([2, Protocol.OK]);
        socket.write(resp);
        console.log(format('[lamp %d] <<< %s'), lamp.id, resp.toString('hex'));
      }

      else if (opcode == Protocol.PING) {
        var resp = new Buffer([2, Protocol.PONG]);
        socket.write(resp);
        console.log(format('[lamp %d] <<< %s'), lamp.id, resp.toString('hex'));
      }

      // Move to next message
      buf.copy(buf, 0, messageLen);
      buflen -= messageLen;
    }
  });

  socket.on('timeout', function () {
    lamps[lamp.id] = undefined;
    socket.end();

    console.log(format('[lamp %d] >>> TIMEOUT!'), lamp.id);
  });
}).listen(50000);

api.listen(8000);

console.log('Server started');
